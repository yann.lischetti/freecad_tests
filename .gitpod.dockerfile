FROM gitpod/workspace-full-vnc

RUN sudo add-apt-repository ppa:freecad-maintainers/freecad-stable \
    && sudo apt-get update \
    && sudo DEBIAN_FRONTEND=noninteractive apt-get install -y \
    python3-pyqt5 \
    freecad=2:0.19.2+dfsg1~202107140649~ubuntu20.04.1
